import React from 'react';
import ReactDom from 'react-dom';
import store from './store';
import './assets/css/bootstrap.min.css';
import './assets/css/style.css';
import Root from "./components/Root";


ReactDom.render( <Root store={store}/>, document.getElementById('root'));
