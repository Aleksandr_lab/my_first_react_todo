import React, {useEffect} from "react";
import WidthServices from '../components/hoc'
import { compose, bindActionCreators } from "redux";
import { connect } from 'react-redux';
import Spinner from "../components/spinner";
import ErrorIndicator from "../components/error-indicator";
import { fetchToDo } from '../actions';
import ToDoList from "../components/todo-list";

const ToDoListContainer = ({fetchToDo, filter, items, loading, error }) => {

    useEffect(() => {
        fetchToDo();
    }, [fetchToDo]);

    if(loading) {
        return <Spinner />
    }

    if(error) {
        return <ErrorIndicator />
    }

    return <ToDoList items={items} filter={filter} />
};

const mapStateToProps = ({toDoLists: {items, loading, error} }, { match: {params} }) => {
    return {
        items,
        loading,
        error,
        filter: params.filter,
    }
};

const mapDispatchToProps = (dispatch, { serivces }) => {
    return  bindActionCreators(
        {
            fetchToDo: fetchToDo(serivces),
        }, dispatch
    );
};

export default compose(
    WidthServices(),
    connect(mapStateToProps, mapDispatchToProps)
)(ToDoListContainer);
