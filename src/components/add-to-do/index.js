import React, { useState } from "react";
import { connect } from 'react-redux';
import { addedToDo } from '../../actions';

const AddToDo = ({addedToDo}) => {
    const [ text, setText ]  = useState('');
    const onSubmit = (e) => {
        e.preventDefault();
        if(text.trim().length) {
            addedToDo(text);
            setText('');
        }
    };
    return (
        <form className="row my-4" onSubmit={onSubmit}>
            <div className="form-group col-md-8 col-xl-10">
                <input type="text"
                       value={text}
                       onChange={(e) => setText(e.target.value)}
                       className="form-control"
                       placeholder="What needs to be done?"
                />
            </div>
            <div className="form-group col-md-4 text-right col-xl-2">
                <button type="submit" className="btn btn-primary w-100">Add</button>
            </div>
        </form>
    )
};

const mapStateToProps = () => ({});

const mapDispatchProps = {
    addedToDo
};

export default connect(mapStateToProps, mapDispatchProps)(AddToDo);
