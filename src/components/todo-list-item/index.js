import React from "react";
import { connect } from 'react-redux';
import * as actions from '../../actions/actionsToDoItems';

const ToDoListItem = ({idx, item, updatePropertyToDo, onDelete}) => {

    const { title, id, important, status } = item;
    let classTitle = 'col-md-8 py-2 font-weight-bold btn text-left';

    if(important) {
        classTitle += ' text-success';
    }
    if(!status) {
        classTitle += ' text-del'
    }
    return (
        <li className="list-group-item d-flex flex-wrap align-items-center">
            <div
                className={classTitle}
                onClick={() => updatePropertyToDo({id, property: 'status'})}>
                    <span className="d-inline-block mr-4">{idx + 1}</span>
                    {title}
            </div>
            <div className="col-md-4 py-2 text-right">
                <button
                    onClick={() => onDelete(id)}
                    style={{width: 35, height: 35}}
                    className="btn btn-outline-danger btn-lg mx-3 p-0">
                    <i className="fa fa-trash-o" />
                </button>
                <button
                    onClick={() => updatePropertyToDo({id, property:  'important'})}
                    style={{width: 35, height: 35}}
                    className="btn btn-outline-success btn-lg p-0">
                    <i className="fa fa-exclamation" />
                </button>
            </div>
        </li>
    )
};

const mapStateToProps = (state, { item, idx }) => ({
    item,
    idx
});

export default connect(mapStateToProps, actions)(ToDoListItem);
