import React from 'react';
import ErrorImg from '../../assets/img/error-img.png'

const ErrorIndicator = () => (
    <div className="card text-white bg-danger h-100">
        <div className="card-header text-center">
            <img src={ErrorImg} width={100} alt="Error" />
        </div>
        <div className="card-body text-center">
            <h4 className="card-title">Sorry something went wrong</h4>
            <p className="card-text">The developer is already working on fixing bugs.</p>
        </div>
    </div>
);

export default ErrorIndicator;
