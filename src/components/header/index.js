import React from "react";
import { connect } from 'react-redux';

const Header = ({allTodo, doneTodo}) => (
    <div className="row align-items-baseline">
        <div className="col-md-6 py-1">
            <span className="display-3">To do List</span>
        </div>
        <div className="col-md-6 text-right py-1">
            <span className="h4">{allTodo} more to do, {doneTodo} done</span>
        </div>
    </div>
);

const mapStateToProps = ({fillterToDo: {allTodo, doneTodo}}) => ({
    allTodo,
    doneTodo,
});

export default connect(mapStateToProps)(Header);
