import React from "react";
import Header from "../header";
import SearchToDo from "../search-todo";
import { Route, Switch } from 'react-router-dom';
import AddToDo from "../add-to-do";
import ToDoListContainer from "../../containers/ContainerToDo";

const App = () => (
    <div className="container pt-lg-5 pt-md-3 pt-2">
        <Header />
        <SearchToDo />
        <Switch>
            <Route path='/:filter?' component={ToDoListContainer} />
        </Switch>
        <AddToDo />
    </div>
);

export default App;
