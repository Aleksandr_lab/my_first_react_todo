import React from 'react';
import { Provider } from "react-redux";
import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import ErrorBoundry from './error-boundry';
import {ServiceProvider} from './services-context';

import App from './app';
import Services from "../services";
const services = new Services();

const Root = ({ store }) => (
    <Provider store={store}>
        <ErrorBoundry>
            <ServiceProvider value={services}>
                <Router>
                    <App/>
                </Router>
            </ServiceProvider>
        </ErrorBoundry>
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired
};

export default Root
