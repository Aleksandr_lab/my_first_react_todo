import React from "react";
import { updateSearchText } from '../../actions';
import { connect } from 'react-redux';
import FilleterToDo from "../fillter-to-do";

const SearchToDo = ({searchText, updateSearchText}) =>  (
    <div className="row pb-2 mt-4">
        <div className="col-md-8 pb-3">
            <input
                value={searchText}
                onChange={(e)=>  updateSearchText(e.target.value)}
                type="text"
                placeholder="Type to search"
                className="form-control" />
        </div>
        <div className="col-md-4 pb-3 text-right">
            <FilleterToDo />
        </div>
    </div>
);

const mapStateToProps = ({fillterToDo: {searchText}}) => ({
    searchText
});

const mapDispatchToProps = {
    updateSearchText
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchToDo);
