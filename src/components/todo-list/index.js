import React, { useEffect } from "react";
import ToDoListItem from "../todo-list-item";
import { connect } from 'react-redux';
import { updateHeader } from '../../actions';

const ToDoList = ({items, updateHeader, filter, searchText}) => {
    useEffect(()=>{
        updateHeader(items);
    }, [items, updateHeader]);
    let toDoList = [...items, ...[]];
    if(filter) {
        if(filter === 'done') {
            toDoList = toDoList.filter((el) => !el.status);
        }
        if(filter === 'active') {
            toDoList = toDoList.filter((el) => el.status)
        }
    }
    if(searchText) {
        const removeExcess = (str) => str.replace(/\s/g, '').toLowerCase();
        toDoList = toDoList.filter((el) => removeExcess(el.title).includes(removeExcess(searchText)))
    }

    return (
        <ul className="list-group">
            { toDoList.map((item, i) => <ToDoListItem key={item.id} idx={i} item={item} />) }
        </ul>
    )
};

const mapStateToProps = ({ fillterToDo: { searchText } }, {items, filter}) => ({
    items,
    filter,
    searchText,
});

export default connect(mapStateToProps, { updateHeader })(ToDoList);
