import React from "react";
import { ServiceConsumer } from '../services-context';

const WidthServices = () => (Wrapped) => (props) => (
    <ServiceConsumer>
        {
            (serivices) => <Wrapped {...props} serivces={serivices} />
        }
    </ServiceConsumer>
);

export default WidthServices;
