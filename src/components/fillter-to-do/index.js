import React from "react";
import { NavLink } from "react-router-dom";

const FilleterToDo = () => (
    <div className="btn-group" role="group">
        <NavLink to="/" exact className="btn btn-primary">All</NavLink>
        <NavLink to="/active/" className="btn btn-primary">Active</NavLink>
        <NavLink to="/done/" className="btn btn-primary">Done</NavLink>
    </div>
);

export default FilleterToDo;
