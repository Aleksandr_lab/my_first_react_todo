const onDelete = (payload) =>  ({type:'DELETE_TO_DO', payload});
const updatePropertyToDo = (payload) =>  ({type:'UPDATE_PROPERTY_TO_DO', payload});

export {
    onDelete,
    updatePropertyToDo
};
