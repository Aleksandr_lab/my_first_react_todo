const toDoRequested = () => ({type: 'FETCH_TODO_REQUEST'});
const toDoLoaded = (payload) => ({type: 'FETCH_TODO_SUCCESS', payload});
const toDoError = (payload) => ({type: 'FETCH_TODO_FAILURE', payload});
const updateSearchText = (payload) => ({type:'UPDATE_SEARCH_TEXT', payload});
const addedToDo = (payload) => ({type:'ADDED_TODO', payload});
const updateHeader = (payload) => ({type:'UPDATE_HEADER', payload});

const fetchToDo = (services) => () => (dispatch) => {
    dispatch(toDoRequested());
    services.getServices()
        .then((data) => dispatch(toDoLoaded(data)))
        .catch((err) => dispatch(toDoError(err)))
};

export {
    updateSearchText,
    fetchToDo,
    addedToDo,
    updateHeader,
};
