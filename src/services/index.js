export default class Services {
    getServices() {
       return new Promise((resolve, reject) => {
          setTimeout(() => {
              if (Math.random() > .75) {
                  return reject(new Error('Something went wrong!'))
              }
             const storeToDo = localStorage.getItem('toDo');
              return  resolve(storeToDo ? JSON.parse(storeToDo) : [])
          }, 500);
       });
    }

}
