import { v4 as uuidv4 } from 'uuid';

const updateStore = (items) =>  {
    localStorage.setItem('toDo', JSON.stringify(items));
    return items;
};

const createItem = (items, title) => {
    const toDo = {
        id: uuidv4(),
        title,
        status: true,
        important: false,
    };
    return updateStore([
        ...items,
        toDo
    ]);
};

const updateItem = (items, {id, property}) => {
   const idx = items.findIndex((el) => el.id === id);
   const newItem = {
       ...items[idx],
     [property]: !items[idx][property]
   };
    return updateStore([
        ...items.slice(0, idx),
        newItem,
        ...items.slice(idx+1)
    ])
};

const removeToDo = (items, id) => {
    const idx = items.findIndex((el) => el.id === id);
    return updateStore([
        ...items.slice(0, idx),
        ...items.slice(idx+1)
    ])
};

const updateToDo = (state, { type, payload }) => {
    if(state === undefined) {
        return {
            items: [],
            loading: true,
            error: null,
        }
    }

    const { toDoLists } = state;
    switch (type) {
        case 'FETCH_TODO_REQUEST':
            return {
                items: [],
                loading: true,
                error: null,
            };
        case 'FETCH_TODO_SUCCESS':
            return {
                items: payload,
                loading: false,
                error: null,
            };
        case 'FETCH_TODO_FAILURE':
            return {
                items: [],
                loading: false,
                error: payload,
            };
        case 'ADDED_TODO':
            return {
                ...toDoLists,
                items: createItem(toDoLists.items, payload)
            };
        case 'UPDATE_PROPERTY_TO_DO':
            return {
                ...toDoLists,
                items: updateItem(toDoLists.items, payload)
            };
        case 'DELETE_TO_DO':
            return {
                ...toDoLists,
                items: removeToDo(toDoLists.items, payload)
            };
        default:
            return toDoLists
    }
};

export default updateToDo;
