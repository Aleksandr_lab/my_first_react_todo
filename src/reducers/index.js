import updateToDo from './updateToDo';
import updateFilterToDo from './updateFilterToDo';

const reducer = (state, action) => {
    return {
        toDoLists: updateToDo(state, action),
        fillterToDo: updateFilterToDo(state, action),
    }
};

export default reducer;
