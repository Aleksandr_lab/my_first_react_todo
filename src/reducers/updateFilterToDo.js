const updateFilterToDo = (state, { type, payload}) => {
    if(state === undefined) {
        return {
            searchText: '',
            allTodo: 0,
            doneTodo: 0,
        }
    }
    switch (type) {
        case 'UPDATE_SEARCH_TEXT':
            return {
                ...state.fillterToDo,
                searchText: payload
            };
        case 'UPDATE_HEADER':
            return {
                ...state.fillterToDo,
                allTodo: payload.length,
                doneTodo: payload.filter(el => !el.status).length
            };
        default:
            return state.fillterToDo
    }
};

export default updateFilterToDo;
